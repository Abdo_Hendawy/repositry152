package eg.edu.alexu.csd.oop.game.sample.object;

import eg.edu.alexu.csd.oop.game.GameObject;



import java.util.LinkedList;
import java.util.List;

public class VanishSimilarPlates {


    private List<GameObject> onBar = new LinkedList();



    public VanishSimilarPlates(List<GameObject> onBar) {
        this.onBar = onBar;
    }
    PlatesIntersection instance = new PlatesIntersection(onBar);
    int n = onBar.size();
    public void vanish (){
        if (instance.isSimilar()){
            ((ImageObject) onBar.get(n-1)).setVisible(false);
            onBar.remove(onBar.get(n-1));
            ((ImageObject) onBar.get(n-2)).setVisible(false);
            onBar.remove(onBar.get(n-2));
            ((ImageObject) onBar.get(n-3)).setVisible(false);
            onBar.remove(onBar.get(n-3));
        }
    }
}
